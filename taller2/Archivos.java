package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float[] result = new float[largo];

        for (int i = 0; i < largo; i++) {
            result[i] = entrada.nextFloat();
        }

        return result;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] matriz = new float[filas][columnas];

        for (int i = 0; i < filas; i++) {
            matriz[i] = leerVector(entrada, columnas);
        }

        return matriz;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < alto - i - 1; j++) {
                salida.print(" ");
            }
            for (int j = 0; j < 2 * i + 1; j++) {
                salida.print("*");
            }
            for (int j = 0; j < alto - i - 1; j++) {
                salida.print(" ");
            }
            salida.println();
        }
    }
}
