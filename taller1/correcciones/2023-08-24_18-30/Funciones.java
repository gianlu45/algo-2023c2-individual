package aed;

import static java.lang.Math.sqrt;

class Funciones {
    int cuadrado(int x) {
        return x*x;
    }

    double distancia(double x, double y) {
        return sqrt((x*x) + (y*y));
    }

    boolean esPar(int n) {
        int res = n%2;
        return (res == 0);
    }

    boolean esBisiesto(int n) {
        int divideCuatro = n%4;
        int divideCien = n%100;
        int divideCuatrocientos = n%400;
        return (divideCuatro== 0 && divideCien != 0 || divideCuatrocientos == 0);
    }

    int factorialIterativo(int n) {
        int res = 1;
        for(int i = 2; i <= n; i++){
            res = res * i;
        }
        return res;
    }

    int factorialRecursivo(int n) {
        int res = 1;
        if(n != 0){
            res = n * factorialRecursivo(n-1);
        }
        return res;
    }

    boolean esPrimo(int n) {
        int res = 0;
        for(int i = 1; i <= n; i++){
            if(n%i == 0) {
                res = res + 1;
            }
        }
        return (res == 2);
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        for (int numero : numeros) {
            res = res + numero;
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        for (int i = 0; i < numeros.length; i++){
            if (numeros[i] == buscado){
                return i;
            }
        }
        return 0;
    }

    boolean tienePrimo(int[] numeros) {
        boolean res = false;
        int i = 0;
        while( i < numeros.length && !res){
            res = esPrimo(numeros[i]);
            i++;
        }
        return res;
    }

    boolean todosPares(int[] numeros) {
        boolean res = true;
        int i = 0;
        while(i < numeros.length && res){
            res = esPar(numeros[i]);
            i++;
        }
        return res;
    }

    boolean esPrefijo(String s1, String s2) {
        int i = 0;
        while(i < s1.length()){
            if(i < s2.length() && s2.contains(s1)){
                return true;
            }
            i++;
        }
        return false;
    }

    boolean esSufijo(String s1, String s2) {
        String reverseS1 = new StringBuilder(s1).reverse().toString();
        String reverseS2 = new StringBuilder(s2).reverse().toString();
        return esPrefijo(reverseS1, reverseS2);
    }
}

