package aed;

import java.util.ArrayList;
import java.util.List;

public class InternetToolkit {
    public InternetToolkit() {
    }

    public Fragment[] tcpReorder(Fragment[] fragments) {
        for (int i = 1; i < fragments.length; i++) {
            Fragment key = fragments[i];
            int j = i - 1;

            while (j >= 0 && fragments[j].compareTo(key) > 0) {
                fragments[j + 1] = fragments[j];
                j = j - 1;
            }
            fragments[j + 1] = key;
        }
        return fragments;
    }

    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        List<Router> filteredRouters = new ArrayList<>();
        for (Router router : routers) {
            if (router.getTrafico() > umbral) {
                filteredRouters.add(router);
            }
        }
        for (int i = 0; i < filteredRouters.size() - 1; i++) {
            int maxIdx = i;
            for (int j = i + 1; j < filteredRouters.size(); j++) {
                if (filteredRouters.get(j).compareTo(filteredRouters.get(maxIdx)) > 0) {
                    maxIdx = j;
                }
            }
            Router temp = filteredRouters.get(maxIdx);
            filteredRouters.set(maxIdx, filteredRouters.get(i));
            filteredRouters.set(i, temp);
        }
        Router[] topRouters = new Router[Math.min(k, filteredRouters.size())];
        for (int i = 0; i < topRouters.length; i++) {
            topRouters[i] = filteredRouters.get(i);
        }

        return topRouters;
    }


    public IPv4Address[] sortIPv4(String[] ipv4) {
        IPv4Address[] ipv4Addresses = new IPv4Address[ipv4.length];
        for (int i = 0; i < ipv4.length; i++) {
            ipv4Addresses[i] = new IPv4Address(ipv4[i]);
        }

        boolean swapped;
        for (int i = 0; i < ipv4Addresses.length - 1; i++) {
            swapped = false;
            for (int j = 0; j < ipv4Addresses.length - i - 1; j++) {
                if (compareIPv4Addresses(ipv4Addresses[j], ipv4Addresses[j + 1]) > 0) {
                    IPv4Address temp = ipv4Addresses[j];
                    ipv4Addresses[j] = ipv4Addresses[j + 1];
                    ipv4Addresses[j + 1] = temp;
                    swapped = true;
                }
            }
            if (!swapped)
                break;
        }

        return ipv4Addresses;
    }

    private int compareIPv4Addresses(IPv4Address ip1, IPv4Address ip2) {
        for (int i = 0; i < 4; i++) {
            int diff = ip1.getOctet(i) - ip2.getOctet(i);
            if (diff != 0) {
                return diff;
            }
        }
        return 0;
    }

}
