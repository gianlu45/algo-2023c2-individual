package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {

    private class Nodo {
        T dato;
        Nodo siguiente;
        Nodo anterior;

        public Nodo(T dato) {
            this.dato = dato;
            this.siguiente = null;
            this.anterior = null;
        }
    }

    private Nodo cabeza;
    private Nodo cola;
    private int size;

    public ListaEnlazada() {
        this.cabeza = null;
        this.cola = null;
        this.size = 0;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        this();
        Nodo actual = lista.cabeza;
        while (actual != null) {
            this.agregarAtras(actual.dato);
            actual = actual.siguiente;
        }
    }

    public int longitud() {
        return size;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevo = new Nodo(elem);
        if (cabeza == null) {
            cabeza = cola = nuevo;
        } else {
            nuevo.siguiente = cabeza;
            cabeza.anterior = nuevo;
            cabeza = nuevo;
        }
        size++;
    }

    public void agregarAtras(T elem) {
        Nodo nuevo = new Nodo(elem);
        if (cola == null) {
            cabeza = cola = nuevo;
        } else {
            cola.siguiente = nuevo;
            nuevo.anterior = cola;
            cola = nuevo;
        }
        size++;
    }

    public T obtener(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException();
        }
        Nodo actual = cabeza;
        for (int idx = 0; idx < i; idx++) {
            actual = actual.siguiente;
        }
        return actual.dato;
    }

    public void eliminar(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException();
        }

        Nodo actual = cabeza;

        if (size == 1) {
            cabeza = null;
            cola = null;
        } else if (i == 0) {
            cabeza = cabeza.siguiente;
            cabeza.anterior = null;
        } else {
            for (int idx = 0; idx < i; idx++) {
                actual = actual.siguiente;
            }
            if (actual == cola) {
                cola = actual.anterior;
                cola.siguiente = null;
            } else {
                actual.anterior.siguiente = actual.siguiente;
                actual.siguiente.anterior = actual.anterior;
            }
        }
        size--;
    }

    public void modificarPosicion(int indice, T elem) {
        if (indice < 0 || indice >= size) {
            throw new IndexOutOfBoundsException();
        }

        Nodo actual = cabeza;
        for (int idx = 0; idx < indice; idx++) {
            actual = actual.siguiente;
        }
        actual.dato = elem;
    }

    public ListaEnlazada<T> copiar() {
        return new ListaEnlazada<T>(this);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        Nodo actual = cabeza;
        while (actual != null) {
            sb.append(actual.dato.toString());
            actual = actual.siguiente;
            if (actual != null) {
                sb.append(", ");
            }
        }
        sb.append(']');
        return sb.toString();
    }

    private class ListaIterador implements Iterador<T> {
        private Nodo actual;
        private Nodo previo;

        public ListaIterador() {
            this.actual = cabeza;
            this.previo = null;
        }

        public boolean haySiguiente() {
            return actual != null;
        }

        public boolean hayAnterior() {
            return previo != null;
        }

        public T siguiente() {
            if (actual == null) {
                throw new NoSuchElementException("No hay más elementos en la lista.");
            }
            T dato = actual.dato;
            previo = actual;
            actual = actual.siguiente;
            return dato;
        }

        public T anterior() {
            if (previo == null) {
                throw new NoSuchElementException("No hay elementos anteriores en la lista.");
            }
            T dato = previo.dato;
            actual = previo;
            previo = previo.anterior;
            return dato;
        }
    }

        public Iterador<T> iterador() {
            return new ListaIterador();
        }
    }


