package aed;

import java.util.*;

public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private class Nodo {
        T valor;
        Nodo izquierdo, derecho, padre;

        Nodo(T valor) {
            this.valor = valor;
        }
    }

    private Nodo raiz;
    private int size;

    public ABB() {
        this.raiz = null;
        this.size = 0;
    }

    @Override
    public int cardinal() {
        return this.size;
    }

    @Override
    public T minimo() {
        Nodo current = raiz;
        while (current.izquierdo != null) {
            current = current.izquierdo;
        }
        return current.valor;
    }

    @Override
    public T maximo() {
        Nodo current = raiz;
        while (current.derecho != null) {
            current = current.derecho;
        }
        return current.valor;
    }

    @Override
    public void insertar(T elem) {
        raiz = insertarRec(raiz, elem, null);
    }

    private Nodo insertarRec(Nodo raiz, T elem, Nodo padre) {
        if (raiz == null) {
            size++;
            Nodo nuevoNodo = new Nodo(elem);
            nuevoNodo.padre = padre;
            return nuevoNodo;
        }
        if (elem.compareTo(raiz.valor) < 0) {
            raiz.izquierdo = insertarRec(raiz.izquierdo, elem, raiz);
        } else if (elem.compareTo(raiz.valor) > 0) {
            raiz.derecho = insertarRec(raiz.derecho, elem, raiz);
        }
        return raiz;
    }

    @Override
    public boolean pertenece(T elem) {
        return perteneceRec(raiz, elem);
    }

    private boolean perteneceRec(Nodo raiz, T elem) {
        if (raiz == null) return false;
        if (elem.compareTo(raiz.valor) < 0) return perteneceRec(raiz.izquierdo, elem);
        if (elem.compareTo(raiz.valor) > 0) return perteneceRec(raiz.derecho, elem);
        return true;
    }

    public void eliminar(T elem) {
        raiz = eliminarRec(raiz, elem);
    }

    private Nodo eliminarRec(Nodo raiz, T elem) {
        if (raiz == null) {
            return raiz;
        }

        int compareResult = elem.compareTo(raiz.valor);

        if (compareResult < 0) {
            raiz.izquierdo = eliminarRec(raiz.izquierdo, elem);
        } else if (compareResult > 0) {
            raiz.derecho = eliminarRec(raiz.derecho, elem);
        } else {
            if (raiz.izquierdo == null) {
                size--;
                return raiz.derecho;
            } else if (raiz.derecho == null) {
                size--;
                return raiz.izquierdo;
            }

            raiz.valor = minValor(raiz.derecho);

            raiz.derecho = eliminarRec(raiz.derecho, raiz.valor);
        }

        return raiz;
    }

    private T minValor(Nodo raiz) {
        T minv = raiz.valor;
        while (raiz.izquierdo != null) {
            minv = raiz.izquierdo.valor;
            raiz = raiz.izquierdo;
        }
        return minv;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        toStringRec(raiz, sb);
        if (sb.length() > 1) {
            sb.setLength(sb.length() - 1);
        }
        sb.append("}");
        return sb.toString();
    }

    private void toStringRec(Nodo nodo, StringBuilder sb) {
        if (nodo == null) {
            return;
        }
        toStringRec(nodo.izquierdo, sb);
        sb.append(nodo.valor).append(",");
        toStringRec(nodo.derecho, sb);
    }

    private class ABB_Iterador implements Iterador<T> {
        private Stack<Nodo> stack = new Stack<>();

        public ABB_Iterador() {
            Nodo current = raiz;
            while (current != null) {
                stack.push(current);
                current = current.izquierdo;
            }
        }

        public boolean haySiguiente() {
            return !stack.isEmpty();
        }

        public T siguiente() {
            if (!haySiguiente()) throw new NoSuchElementException();
            Nodo current = stack.pop();
            T result = current.valor;
            if (current.derecho != null) {
                current = current.derecho;
                while (current != null) {
                    stack.push(current);
                    current = current.izquierdo;
                }
            }
            return result;
        }
    }
    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }
}
